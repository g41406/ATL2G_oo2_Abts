package esi.dev3.atl.g41406.oo2;

public class Person {
    private String name;
    private String surname;
    private Date birthDate;
    
    public Person(String name,String surname,Date birthDate){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }
    @Override
    public String toString(){
        return name + " " + surname + " né(e) " + birthDate;
    }
}
