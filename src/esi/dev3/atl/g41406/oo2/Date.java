package esi.dev3.atl.g41406.oo2;

/**
 * Represents a date with its day month and year
 */
public class Date {

    private int day;
    private int month;
    private int year;

    /**
     * Creates a date with its day month and year , if the date isn't valid , an
     * exception will be generated
     *
     * @param day the date day
     * @param month the date month
     * @param year the date year
     */
    public Date(int day, int month, int year) {
        if (!isValid(day, month, year)) {
            throw new IllegalArgumentException("Date isn't valid. ◣_◢");
        }
        this.day = day;
        this.month = month;
        this.year = year;
    }

    /**
     * Tells whether the date year is bissextile or not
     *
     * @return true if it's bissextile , false if it isn't
     */
    private boolean isBissextile() {
        return isBissextile(this.year);
    }

    /**
     * Tells whether a year is bissextile or not
     *
     * @param year the year that has to be tested
     * @return true if it's bissextile , false if it isn't
     */
    private boolean isBissextile(int year) {
        return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
    }

    /**
     * Tells how many days there are in the date month
     *
     * @return the amount of days
     */
    private int daysInMonth() {
        return daysInMonth(this.month, this.year);
    }

    /**
     * Tells how many dayz there are in the given month and year
     *
     * @param month the month of the date
     * @param year the year of the date
     * @return the amount of days in the month for that year
     */
    private int daysInMonth(int month, int year) {
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return (isBissextile(year)) ? 29 : 28;
            default:
                throw new IllegalArgumentException("Month isn't valid , has to be from 1 to 12 ◣_◢");
        }
    }

    /**
     * Tells whether a date is valid or not
     *
     * @param day the day to be tested
     * @param month the month to be tested
     * @param year the year to be tested
     * @return true if it's valid , false if it isn't
     */
    private boolean isValid(int day, int month, int year) {
        return (day > 0 && day <= daysInMonth(month, year))
                && (month > 0 && month < 13)
                && (year > 0 && year < 10000);
    }

    /**
     * Adds a day to the current date , generate an exception if we reach the
     * limit date which is 31/12/9999
     */
    public void increment() {
        if (day == 31 && month == 12 && year == 9999) {
            throw new IllegalArgumentException("Technical issue : this program isn't meant to work with year superior to 9999（︶︿︶)");
        }
        if (day == daysInMonth()) {
            day = 1;
            if (month == 12) {
                ++year;
                month = 1;
            } else {
                ++month;
            }
        } else {
            ++day;
        }
    }

    /**
     * Gives the day number in the year , EG : 15/2/2015 will yield 31 + 15 = 46
     *
     * @return the day number in the year
     */
    public int dayOfYear() {
        int day = this.day;
        for (int i = 1; i < month; ++i) {
            day += daysInMonth(i, year);
        }
        return day;
    }

    /**
     * Gives the day of the week , 0 -> Monsday , etc
     *
     * @return the day of the week
     */
    public int dayOfWeek() {
        int Q = day - 2;
        int Y = (month < 3) ? year - 1 : year;
        int M = (month < 3) ? month + 12 : month;
        return (int) (+Q
                + (int) ((26 * (M + 1)) / 10.0)
                + Y
                + (int) (Y / 4.0)
                + 6 * (int) ((Y / 100.0))
                + (int) (Y / 400.0)) % 7;
    }

    /**
     * Gives the day of the week in letters
     *
     * @return the day of the week in letters
     */
    public String dayToString() {
        switch (dayOfWeek()) {
            case 0:
                return "lundi";
            case 1:
                return "mardi";
            case 2:
                return "mercredi";
            case 3:
                return "jeudi";
            case 4:
                return "vendredi";
            case 5:
                return "samedi";
            default:
                return "dimanche";
        }
    }

    /**
     * Gives the month in letters
     *
     * @return the month in letters
     */
    public String monthToString() {
        switch (month) {
            case 1:
                return "janvier";
            case 2:
                return "février";
            case 3:
                return "mars";
            case 4:
                return "avril";
            case 5:
                return "mai";
            case 6:
                return "joint";
            case 7:
                return "juillet";
            case 8:
                return "aout";
            case 9:
                return "septembre";
            case 10:
                return "octobre";
            case 11:
                return "novembre";
            default:
                return "décembre";
        }
    }

    /**
     * Gives the date in a string
     *
     * @return the date in a string
     */
    @Override
    public String toString() {
        return dayToString() + " " + day + " " + monthToString() + " " + year;
    }

    /**
     * Gives the date day
     *
     * @return the date day
     */
    public int getDay() {
        return day;
    }

    /**
     * Gives the date month
     *
     * @return the date month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Gives the date year
     *
     * @return the date year
     */
    public int getYear() {
        return year;
    }

    /**
     * Set the month to a given month , generate an exception if it causes the
     * date to be invalid
     *
     * @param month the mont to be set
     */
    public void setMonth(int month) {
        if (!isValid(this.day, month, this.year)) {
            throw new IllegalArgumentException("Incorrect date ◣_◢");
        }
        this.month = month;
    }
}
