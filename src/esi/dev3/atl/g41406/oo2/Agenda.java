package esi.dev3.atl.g41406.oo2;

import java.util.LinkedList;

public class Agenda {
    private LinkedList<Meeting> meetings;
    
    public Agenda(){
        meetings = new LinkedList<Meeting>() {};
    }
    public void addMeeting(Meeting meeting){
        meetings.add(meeting);
    }
    public void removeMeeting(int id){
        meetings.remove(id);
    }
    public Meeting getMeeting(int id){
        return meetings.get(id);
    }
    public void display(int id){
        System.out.println(meetings.get(id));
    }
    public void displayAll(){
        for(Meeting meeting : meetings){
            System.out.println(meeting);
        }
    }
    
}
