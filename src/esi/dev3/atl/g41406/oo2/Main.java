package esi.dev3.atl.g41406.oo2;

public class Main {

    public static void main(String[] args) {
        Date date = new Date(15, 9, 2015);
        for (int i = 0; i < 50; ++i) {
            System.out.print(date + ((i%7 == 6) ? "\n" : " ~ ")); 
            date.increment();
        }
        Agenda agenda = new Agenda();
        agenda.addMeeting(
                new Meeting(
                new DatePrecise(25,9,2015,12,0),
                new DatePrecise(25,9,2015,15,0),
                "pas d'idée ツ"
                ));
        System.out.println("\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        agenda.displayAll();
        agenda.getMeeting(0).addPerson(new Person("Gilbert","Michel",new Date(15,3,1980)));
        agenda.getMeeting(0).addPerson(new Person("Pierre","Antoine",new Date(18,2,1984)));
        System.out.println(agenda.getMeeting(0).listOfGuests());
    }
}
