/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.dev3.atl.g41406.oo2;

/**
 *
 * @author T5UN4M1
 */
public class DatePrecise extends Date {
    private int hour;
    private int minute;
    
    public DatePrecise(int day, int month, int year,int hour,int minute) {
        super(day, month, year);
        this.minute = minute;
        this.hour = hour;
    }
    
    @Override
    public String toString(){
        return "Le " + super.toString() + " à " +((hour<10) ? "0" : "") + hour + ":" + ((minute<10) ? "0" : "") + minute;
    }
    
}
