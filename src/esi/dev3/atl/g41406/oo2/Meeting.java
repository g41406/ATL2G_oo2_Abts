package esi.dev3.atl.g41406.oo2;

import java.util.HashSet;
import java.util.Set;

public class Meeting {
    private DatePrecise startDate;
    private DatePrecise endDate;
    private String subject;
    private Set<Person> guests;
    
    public Meeting(DatePrecise startDate,DatePrecise endDate,String subject){
        this.startDate = startDate;
        this.endDate = endDate;
        this.subject = subject;
        guests = new HashSet(); 
    }
    public void addPerson(Person newGuest){
        guests.add(newGuest);
    }
    @Override
    public String toString(){
        return "Ce meeting débute " +startDate + " et se termine " + endDate + ", il a pour sujet " + subject + ".";
    }
    public String listOfGuests(){
        String list = "";
        for(Person guest : guests){
            list += guest + "\n";
        }
        return list;
    }
}


