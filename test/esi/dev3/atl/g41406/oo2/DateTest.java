package esi.dev3.atl.g41406.oo2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author T5UN4M1
 */
public class DateTest {

    /**
     * Test of increment method, of class Date.
     */
    @Test
    public void testIncrement() {
        Date instance = new Date(31, 12, 2000);
        for (int i = 0; i < 5000; ++i) {
            instance.increment();
        }
    }

    /**
     * Test of dayOfYear method, of class Date.
     */
    @Test
    public void testDayOfYear() {
        Date instance = new Date(31, 12, 2008);
        int expResult = 366;
        int result = instance.dayOfYear();
        assertEquals(expResult, result);
    }

    /**
     * Test of dayOfWeek method, of class Date.
     */
    @Test
    public void testDayOfWeek() {
        Date instance = new Date(22, 9, 2015);
        int expResult = 1;
        int result = instance.dayOfWeek();
        assertEquals(expResult, result);
    }

    /**
     * Test of dayOfWeek method, of class Date.
     */
    @Test
    public void testDayOfWeek2() {
        Date instance = new Date(1, 1, 1900);
        int expResult = 0;
        int result = instance.dayOfWeek();
        assertEquals(expResult, result);
        for (int i = 0; i < 2000000; ++i) {
            instance.increment();
            result = instance.dayOfWeek();
            expResult = (++expResult % 7);
            assertEquals(expResult, result);
        }
    }
    /**
     * Tests if exception is thrown
     */
    @Test(expected=IllegalArgumentException.class)
    public void testException1() {
        Date instance = new Date(31,1,10000);
    }
    /**
     * Tests if exception is thrown
     */
    @Test(expected=IllegalArgumentException.class)
    public void testException2() {
        Date instance = new Date(31,12,9999);
        instance.increment();
    }
    /**
     * Tests if exception is thrown
     */
    @Test(expected=IllegalArgumentException.class)
    public void testException3() {
        Date instance = new Date(31,12,9999);
        instance.setMonth(11);
    }
    /**
     * Tests if exception is thrown
     */
    @Test(expected=IllegalArgumentException.class)
    public void testException4() {
        Date instance = new Date(3,12,5000);
        instance.setMonth(13);
    }
}
